Local build
===========
Update settings in application.properties



pom > Run as Maven build; goal = package
war in /target/




Ensure mysql local db is present
Create a new 'dsarepouser':
mysql> CREATE USER 'dsarepouser'@'localhost' IDENTIFIED BY '<pwd>';   <-- user '%' instead of 'localhost' to allow network access
Grant all premissions to that user:
mysql> GRANT ALL PRIVILEGES ON dsarepo.* TO 'dsarepouser'@'localhost';
Flush privileges:
mysql> FLUSH PRIVILEGES;

Import schema from specific file

Local deploy
============
run deploy.bat to create WAR start tomcat
URL /DSAEditor

