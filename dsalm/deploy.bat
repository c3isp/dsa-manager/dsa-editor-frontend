@echo off

set TOMCAT_HOME=C:\marco\dev\apache-tomcat-8.5.29
set LOCAL_WAR=C:\marco\dev\eclipse-ws\dsaEditorFrontend\dsalm\target\DSAEditor.war

rem CALL "%TOMCAT_HOME%\bin\shutdown.bat"
DEL /Q "%TOMCAT_HOME%\logs\*"
if exist "%TOMCAT_HOME%\webapps\DSAEditor.war" DEL "%TOMCAT_HOME%\webapps\DSAEditor.war"
if exist "%TOMCAT_HOME%\webapps\DSAEditor" RMDIR /S /Q "%TOMCAT_HOME%\webapps\DSAEditor"
call mvn clean package
XCOPY "%LOCAL_WAR%" "%TOMCAT_HOME%\webapps\"

call "%TOMCAT_HOME%\bin\startup.bat"

@pause








rem call "C:\apache-tomcat-8.0.50\bin\shutdown.bat"
rem DEL /Q "c:\apache-tomcat-8.0.50\logs\*"
rem DEL "C:\apache-tomcat-8.0.50\webapps\DSAAuthoringTool.war"
rem RMDIR /S /Q "C:\apache-tomcat-8.0.50\webapps\DSAAuthoringTool"
rem call mvn -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true clean install -P local
rem XCOPY "C:\Users\gambardc\git\dsaat\target\DSAAuthoringTool.war" "C:\apache-tomcat-8.0.50\webapps\"
call "C:\apache-tomcat-8.0.50\bin\startup.bat"
