package com.atos.dsalm.ui;


import com.atos.dsalm.crud.DSATableView;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;

@SpringComponent
@UIScope
public class MainScreen extends HorizontalLayout {
	
  
	private Menu menu;
	
    public MainScreen(DsaLmUI ui, SpringViewProvider viewProvider) {
         setStyleName("main-screen");

           CssLayout viewContainer = new CssLayout();
           viewContainer.addStyleName("valo-content");
           viewContainer.setSizeFull();

           final Navigator navigator = new Navigator(ui, viewContainer);
           navigator.addProvider(viewProvider);
           navigator.setErrorView(ErrorView.class);
           menu = new Menu(navigator);
           menu.addView(new DSATableView(), DSATableView.VIEW_NAME,
        		   DSATableView.VIEW_NAME, FontAwesome.EDIT);
           menu.addView(new DsaStatusView(), DsaStatusView.VIEW_NAME, DsaStatusView.VIEW_NAME,
                   FontAwesome.INFO_CIRCLE);
           menu.addView(new AboutView(), AboutView.VIEW_NAME, AboutView.VIEW_NAME,
                   FontAwesome.INFO_CIRCLE);

           navigator.addViewChangeListener(viewChangeListener);

           addComponent(menu);
           addComponent(viewContainer);
           setExpandRatio(viewContainer, 1);
           setSizeFull();
     
    }
    
    // notify the view menu about view changes so that it can display which view
    // is currently active
    ViewChangeListener viewChangeListener = new ViewChangeListener() {

        @Override
        public boolean beforeViewChange(ViewChangeEvent event) {
            return true;
        }

        @Override
        public void afterViewChange(ViewChangeEvent event) {
            menu.setActiveView(event.getViewName());
        }

    };
 

}
