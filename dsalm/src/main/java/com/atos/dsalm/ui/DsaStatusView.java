package com.atos.dsalm.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = DsaStatusView.VIEW_NAME)
public class DsaStatusView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "DSA Status";

    public DsaStatusView() {
        CustomLayout statusContent = new CustomLayout("statusview");
       statusContent.setStyleName("status-content");

       ThemeResource resource = new ThemeResource("img/dsa-status.png");

       // Use the resource
       Image image = new Image(null, resource);
        // you can add Vaadin components in predefined slots in the custom
        // layout
       image.setStyleName("status-image");
             statusContent.addComponent( image , "info"
    		 
              );
        setSizeFull();
        setStyleName("dsa-status-view");
        addComponent(statusContent);

        setComponentAlignment(statusContent, Alignment.MIDDLE_CENTER);
    }

    @Override
    public void enter(ViewChangeEvent event) {
    }

}
