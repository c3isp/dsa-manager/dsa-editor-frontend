package com.atos.dsalm.ui;

import org.springframework.beans.factory.annotation.Autowired;

import com.atos.dsalm.authentication.AccessControl;
import com.atos.dsalm.authentication.BasicAccessControl;
import com.atos.dsalm.authentication.LoginScreen;
import com.atos.dsalm.authentication.LoginScreen.LoginListener;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Viewport;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;



@SpringUI
@Viewport("user-scalable=no,initial-scale=1.0")
@Theme("mytheme")
public class DsaLmUI extends UI {

    @Autowired
    private SpringViewProvider viewProvider;
   

    private AccessControl accessControl = new BasicAccessControl();
    
	@Override
	protected void init(VaadinRequest request) {
		
		    Responsive.makeResponsive(this);
	        setLocale(request.getLocale());
	        getPage().setTitle("DSA Editor");
	        if (!accessControl.isUserSignedIn()) {
	            setContent(new LoginScreen(accessControl, new LoginListener() {
	                @Override
	                public void loginSuccessful() {
	                    showMainView();
	                }
	            }));
	        } else {
	            showMainView();
	        }
	}

 
    
    protected void showMainView() {
    	
    	addStyleName(ValoTheme.UI_WITH_MENU);
    	setContent(new MainScreen(DsaLmUI.this, viewProvider));
    	getNavigator().navigateTo(getNavigator().getState());
       
    }
    
    public static DsaLmUI get() {
        return (DsaLmUI) UI.getCurrent();
    }

    public AccessControl getAccessControl() {
        return accessControl;
    }



}
