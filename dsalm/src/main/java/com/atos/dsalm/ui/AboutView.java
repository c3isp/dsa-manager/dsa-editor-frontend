package com.atos.dsalm.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = AboutView.VIEW_NAME)
public class AboutView extends VerticalLayout implements View {

	public static final String VIEW_NAME = "About";

	public AboutView() {
		CustomLayout aboutContent = new CustomLayout("aboutview");
		aboutContent.setStyleName("about-content");

		// you can add Vaadin components in predefined slots in the custom
		// layout
		
		aboutContent.addComponent(new Label(
				"C3ISP mission is to define a collaborative and confidential information sharing, analysis and protection framework as a service for cyber security management. C3ISP innovation is the possibility to share information in a flexible and controllable manner inside a collaborative multi-domain environment to improve detection of cyber threats and response capabilities,"
				+ " still preserving the confidentiality of the shared information. C3ISP paradigm is collect, analyse, inform, and react. <br> Contains code by ATOS released under Apache 2.0 license for Coco Cloud FP7 EU Project",
				ContentMode.HTML), "info");
		
		setSizeFull();
		setStyleName("about-view");
		addComponent(aboutContent);
		setComponentAlignment(aboutContent, Alignment.MIDDLE_CENTER);
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}

}
