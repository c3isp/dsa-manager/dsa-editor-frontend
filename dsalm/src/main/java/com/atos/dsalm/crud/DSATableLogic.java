package com.atos.dsalm.crud;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.atos.dsalm.authentication.AccessControl;
import com.atos.dsalm.authentication.BasicAccessControl;
import com.atos.dsalm.backend.DataService;
import com.atos.dsalm.backend.data.AnalysisResult;
import com.atos.dsalm.backend.data.DSADetails;
import com.atos.dsalm.backend.data.DSATempDetails;
import com.atos.dsalm.backend.data.ResultObject;
import com.atos.dsalm.objects.UserDetails;
import com.atos.dsalm.ui.DsaLmUI;
import com.fasterxml.jackson.annotation.JsonValue;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.server.Page;
import com.vaadin.server.Sizeable;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

import elemental.json.JsonArray;
import elemental.json.JsonObject;
import elemental.json.impl.JsonUtil;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * This class provides an interface for the logical operations between the CRUD
 * view, its parts like the product editor form and the data source, including
 * fetching and saving products.
 *
 * Having this separate from the view makes it easier to test various parts of
 * the system separately, and to e.g. provide alternative views for the same
 * data.
 */


public class DSATableLogic {
	
	Logger logger = Logger.getLogger("DSATableLogic.class");
	
	private AccessControl accessControl = new BasicAccessControl();
    private DSATableView view;

	public String coflictsText;

    public DSATableLogic(DSATableView DSATableView) {
    	logger.info("DSATableLogic constructor");
        view = DSATableView;
    }

    public void init() {
    	logger.info("DSATableLogic init");
        editProduct(null);
        logger.log(Level.INFO,"***********Entering init with userId "+accessControl.getPrincipalName().getUserId()+"***********");	
        view.showDsas(DataService.get().getAllDsas(accessControl.getPrincipalName().getUserId()));
    }

    public void cancelProduct() {
        setFragmentParameter("");
        view.clearSelection();
        view.editDsa(null);
    }

    /**
     * Update the fragment without causing navigator to change view
     */
    private void setFragmentParameter(String dsaId) {
    	logger.log(Level.INFO,"***********Entering setFragmentParameter dsaId: "+dsaId+"***********");	 
        String fragmentParameter;
        if (dsaId == null || dsaId.isEmpty()) {
            fragmentParameter = "";
        } else {
            fragmentParameter = dsaId;
        }

        Page page = DsaLmUI.get().getPage();
        page.setUriFragment("!" + DSATableView.VIEW_NAME + "/"
                + fragmentParameter, false);
    }

    public void enter(String dsaId) {
    	logger.log(Level.INFO,"***********Entering enter dsaId: "+dsaId+"***********");	 
        if (dsaId != null && !dsaId.isEmpty()) {
            if (dsaId.equals("new")) {
                newProduct();
            } else {
                // Ensure this is selected even if coming directly here from
                // login
                try {
                    
                    DSADetails dsa = findDsa(dsaId, accessControl.getPrincipalName().getUserId());
                    view.selectRow(dsa);
                } catch (NumberFormatException e) {
                }
            }
        }
    }

    private DSADetails findDsa(String dsaId, String userId) {
        return DataService.get().getDsaById(dsaId,userId);
    }

    public void saveProduct(DSADetails dsa) {
    	logger.log(Level.INFO,"***********saveProduct");	
        view.showSaveNotification(dsa.getDsaName() + " ("
                + dsa.getDsaId() + ") updated");
        view.clearSelection();
        view.editDsa(null);
        view.refreshDsa(dsa);
        setFragmentParameter("");
    }

    public void deleteDsa(DSADetails dsa) {
        DataService.get().deleteDsa(dsa.getDsaId());
        view.showSaveNotification(dsa.getDsaName() + " (" + dsa.getDsaId() + ") removed");

        view.clearSelection();
        view.editDsa(null);
        view.removeDsa(dsa);	
        setFragmentParameter("");
    }
    
    public void showDsaAssignment() {
    	
    	MySub2 sub = new MySub2();
		sub.setModal(true);
		sub.setHeight("400px");
		sub.setWidth("400px");
		sub.center();
		
		// Add it to the root component
		UI.getCurrent().addWindow(sub);
		sub.addCloseListener(new CloseListener() {
			private static final long serialVersionUID = -4381415904461841881L;

			public void windowClose(CloseEvent e) {
				view.clearSelection();
				view.editDsa(null);
				setFragmentParameter("");
			}
		});
    }
    
    public void showConflicts(String dsaid) {
       
    	ResultObject myConflicts = DataService.get().getDsaConflicts(dsaid);
        
     //   view.showSaveNotification(myConflicts.getMessage());
    	
    	if (myConflicts.getStatus()) {
			//coflictsText = "{  \"dsaid\": \"string\",  \"policy\": [    {      \"auth\": \"string\",      \"context\": \"string\",      \"prohib\": \"string\"    }  ],  \"status\": \"string\"}";
			
    		coflictsText = myConflicts.getMessage();
    		
    		
    		MySub sub = new MySub();
			sub.setModal(true);
			sub.setHeight("600px");
			sub.setWidth("800px");
			sub.center();
			// Add it to the root component
			UI.getCurrent().addWindow(sub);
			sub.addCloseListener(new CloseListener() {
				private static final long serialVersionUID = -4381415904461841881L;

				public void windowClose(CloseEvent e) {
					view.clearSelection();
					view.editDsa(null);
					setFragmentParameter("");
				}
			});
		}
        
        
    }
    
    public void showRawDsa(String dsaid) {
        
        ResultObject rawDsa = DataService.get().getShowRaw(dsaid);
        DSADetails dsa = findDsa(dsaid, accessControl.getPrincipalName().getUserId());
        logger.info("DSA="+dsa);
        logger.info("DSA id="+dsa.getDsaId());
        view.showRawDsa(dsa, rawDsa.getMessage());
    }
    
    public void editProduct(DSADetails dsa) {
    	logger.log(Level.INFO,"***********rowSelected admin");	 

        if (dsa == null) {
            setFragmentParameter("");
        } else {
            setFragmentParameter(dsa.getDsaId() + "");
        }
        view.editDsa(dsa);
    }

    public void newProduct() {
    	logger.log(Level.INFO,"***********newProduct ");	 

        view.clearSelection();
        setFragmentParameter("new");
        view.editDsa(new DSADetails());
    }

    public void rowSelected(DSADetails dsa) {
    	logger.log(Level.INFO,"***********rowSelected ");	 
        if (DsaLmUI.get().getAccessControl().isUserInRole("admin")) {
        	logger.log(Level.INFO,"***********rowSelected admin");	 
            view.editDsa(dsa);
        }
    }
    
    // Define a sub-window by inheritance
    class MySub2 extends Window {
        /**
	 * 
	 */
	private static final long serialVersionUID = 5666790691113486381L;

		public MySub2() {
            super("DSA Assignment"); // Set window caption
            
            // Some basic content for the window
            VerticalLayout content = new VerticalLayout();
            VerticalLayout mybox = new VerticalLayout();
            
            ComboBox dsaCombo = new ComboBox();
            ComboBox userCombo = new ComboBox();
            
      
            
            Collection<DSADetails> myDSAs =  DataService.get().getAllDsas(accessControl.getPrincipalName().getUserId());
            
            for (Iterator iterator = myDSAs.iterator(); iterator.hasNext();) {
            	DSADetails dsaDetails = (DSADetails) iterator.next();
    			
    			dsaCombo.addItem(dsaDetails.getDsaId());
    			dsaCombo.setItemCaption(dsaDetails.getDsaId(), dsaDetails.getDsaName());
    			
    		} 
            
            Collection<UserDetails> myUsers =  DataService.get().getAllEndUsers();
            
            for (Iterator iterator = myUsers.iterator(); iterator.hasNext();) {
            	UserDetails myUserDetails = (UserDetails) iterator.next();
    			
    			userCombo.addItem(myUserDetails.getUserName());
    			userCombo.setItemCaption(myUserDetails.getUserName(), myUserDetails.getUserName());
    			
    		} 
            
            dsaCombo.setInputPrompt("Select DSA");
            dsaCombo.setFilteringMode(FilteringMode.CONTAINS);
            
            userCombo.setInputPrompt("Select User");
            userCombo.setFilteringMode(FilteringMode.CONTAINS);
            
            Button assignButton = new Button("Assign");
            
           
            
            userCombo.setValidationVisible(false);
            userCombo.setRequired(true);                
            userCombo.setRequiredError("Select A User!"); 
            
            dsaCombo.setValidationVisible(false);
            dsaCombo.setRequired(true);                
            dsaCombo.setRequiredError("Select A DSA!"); 
 
            
    		assignButton.addClickListener(new ClickListener() {
                @Override
                public void buttonClick(ClickEvent event) {
                	
                try {
					if(dsaCombo.isValid() && userCombo.isValid())
					{
						logger.log(Level.INFO,"***********DSA: "+dsaCombo.getValue().toString().trim());
						logger.log(Level.INFO,"***********User: "+userCombo.getValue().toString().trim());	

						ResultObject result = DataService.get().addDsaToUser(dsaCombo.getValue().toString().trim(),userCombo.getValue().toString().trim());   
						if (result.getStatus())
						{
					    	close(); // Close the sub-window
					    	Notification.show("DSA added to the selected user.");
						}
						else
						{
					    	close(); // Close the sub-window
					    	Notification.show("DSA assignment failed!");
						}
					}
					else
					{
						try {
							dsaCombo.validate();
							userCombo.validate();
					    } catch (InvalidValueException e) {
					        Notification.show(e.getMessage());
					    }
					}
				} catch (Exception e) {
		        	logger.log(Level.SEVERE,"***********assignButton error: "+e.getMessage());	 

					e.printStackTrace();
				}
                	
                }
            });
            
            userCombo.setWidth(200, Unit.PIXELS);
            dsaCombo.setWidth(200, Unit.PIXELS);
            assignButton.setWidth(200, Unit.PIXELS);
            
            userCombo.addStyleName("my-top-margin");
            dsaCombo.addStyleName("my-top-margin");
            assignButton.addStyleName("my-top-margin");
            
            mybox.addComponent(dsaCombo);
            mybox.addComponent(userCombo);
            mybox.addComponent(assignButton);
            
            
            mybox.setComponentAlignment(dsaCombo, Alignment.MIDDLE_CENTER);
            mybox.setComponentAlignment(userCombo, Alignment.MIDDLE_CENTER);
            mybox.setComponentAlignment(assignButton, Alignment.MIDDLE_CENTER);
            
            content.setMargin(true);
            
            content.addComponent(mybox);
            content.setSizeFull();
            content.setComponentAlignment(mybox, Alignment.MIDDLE_CENTER);
            
            setContent(content);

        }
    }
    
 // Define a sub-window by inheritance
    class MySub extends Window {
        /**
	 * 
	 */
	private static final long serialVersionUID = 5666790691113486381L;

		public MySub() {
            super(); // Set window caption
            
            // Some basic content for the window
            VerticalLayout content = new VerticalLayout();
            
            JsonObject object = JsonUtil.parse(coflictsText);	            
             
            ObjectMapper myMapper = new ObjectMapper();
            AnalysisResult myResult = new AnalysisResult();
			try {
				 myResult = (AnalysisResult) myMapper.readValue(coflictsText,AnalysisResult.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
         // Create the area
            TextArea area = new TextArea("DSA Conflicts");
            // Put some content in it
            area.setValue(JsonUtil.stringify(object,2));
            area.setHeight(500, Unit.PIXELS);
            area.setWidth(500, Unit.PIXELS);
            
            area.setStyleName("my-text-area");
                        
        Table table = new Table("Conflicts");

        // Define two columns for the built-in container
         table.addContainerProperty("Number", Integer.class, null);
         table.addContainerProperty("Auth", String.class, null);
         table.addContainerProperty("Prohib",  String.class, null);
         table.addContainerProperty("Obli",  String.class, null);
         table.addContainerProperty("Context", String.class, null);
         table.setWidth("100%");
         table.setStyleName("wordwrap-table");
         for(int i=0;i < myResult.getPolicy().size();i++)
         {        	 
        	  // Add a few other rows using shorthand addItem()
             table.addItem(new Object[]{i+1,myResult.getPolicy().get(i).getAuth(), myResult.getPolicy().get(i).getProhib(),myResult.getPolicy().get(i).getObli(),myResult.getPolicy().get(i).getContext()  }, i);
         }
            
         // Show exactly the currently contained rows (items)
         table.setPageLength(table.size());
    //     table.setStyleName("my-text-area");
         
            content.addComponent(table);
            
            content.setMargin(true);
            
           
            setContent(content);

        }
    }
}
