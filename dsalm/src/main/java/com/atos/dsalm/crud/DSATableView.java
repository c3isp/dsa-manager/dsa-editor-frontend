package com.atos.dsalm.crud;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.atos.dsalm.Configurator;
import com.atos.dsalm.SpringContextBridge;
import com.atos.dsalm.authentication.AccessControl;
import com.atos.dsalm.authentication.BasicAccessControl;
import com.atos.dsalm.backend.DataService;
import com.atos.dsalm.backend.data.DSADetails;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Sizeable;
import com.vaadin.server.StreamResource;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid.SelectionModel;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */

@SpringView(name = DSATableView.VIEW_NAME)
public class DSATableView extends CssLayout implements View {
	
	Logger logger = Logger.getLogger("DSATableView.class");
	
    // get configurator for properties
    private Configurator cfg = SpringContextBridge.services().getConfigurator();
    private String dsaAtUrl = cfg.getProperty("dsalm.dsaat.url");
    private String dsaEditorFeUrl = cfg.getProperty("dsalm.dsaeditorfe.url");
	
	private AccessControl accessControl = new BasicAccessControl();
	
    public static final String VIEW_NAME = "DSAs List";
    private DSAGrid grid;
    private DSAForm form;

    private DSATableLogic viewLogic = new DSATableLogic(this);
    private Button newProduct;
    private Button addDsaToUser;
    
    public DSATableView() {
        setSizeFull();
        addStyleName("crud-view");
        HorizontalLayout topLayout = createTopBar();

        grid = new DSAGrid();
        grid.addSelectionListener(new SelectionListener() {

            @Override
            public void select(SelectionEvent event) {
                viewLogic.rowSelected(grid.getSelectedRow());
            }
        });

        form = new DSAForm(viewLogic);

        VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.addComponent(topLayout);
        barAndGridLayout.addComponent(grid);
        barAndGridLayout.setMargin(true);
        barAndGridLayout.setSpacing(true);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.setExpandRatio(grid, 1);
        barAndGridLayout.setStyleName("crud-main-layout");

        Label myLabel = new Label("This project is funded under the European Union’s Horizon 2020 Programme (H2020) - Grant Agreement n. 700294");
       
        myLabel.setStyleName("mycenter");
        
        barAndGridLayout.addComponent(myLabel);
        
        addComponent(barAndGridLayout);
        addComponent(form);

        viewLogic.init();
    }

    public HorizontalLayout createTopBar() {
        TextField filter = new TextField();
        filter.setStyleName("filter-textfield");
        filter.setInputPrompt("Filter by DSA Name, ID, or Status");
      //  ResetButtonForTextField.extend(filter);
        filter.setImmediate(true);
        filter.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            public void textChange(FieldEvents.TextChangeEvent event) {
                grid.setFilter(event.getText());
            }
        });
        
        if(accessControl.getPrincipalName().getUserRole().equalsIgnoreCase("legalexpert"))
        {
        	  newProduct = new Button("New DSA Template");
        }
        else
        {
        	  newProduct = new Button("New DSA");
        }
      
        if (accessControl.getPrincipalName().getUserRole().equalsIgnoreCase("endUser"))
    	{
        	newProduct.setVisible(false);
    	}
       
        newProduct.addStyleName(ValoTheme.BUTTON_PRIMARY);
        newProduct.setIcon(FontAwesome.PLUS_CIRCLE);
        newProduct.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	
            	if(accessControl.getPrincipalName().getUserRole().equalsIgnoreCase("legalexpert"))
            	{
            		String userRoleHPMapping =getMappedRole(accessControl.getPrincipalName().getUserRole());
                	String myUser=accessControl.getPrincipalName().getUserId();
                	String mySelectedDSA = "";
                	getUI().getPage().setLocation(dsaAtUrl+"/?issuer="+userRoleHPMapping+"&userID="+myUser+"&dsaId=&action=Create&target="+dsaEditorFeUrl);

            	}
            	else if (accessControl.getPrincipalName().getUserRole().equalsIgnoreCase("policyExpert"))
            	{
            	    form.addStyleName("visible");
                    form.setEnabled(true);
                    hideOtherButtons();
            	}
            
            }
        });
        
        
        addDsaToUser = new Button("Assign DSA's");
        addDsaToUser.addStyleName(ValoTheme.BUTTON_PRIMARY);
        addDsaToUser.setIcon(FontAwesome.PLUS_CIRCLE);
        addDsaToUser.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	
            
            	viewLogic.showDsaAssignment();
            
            
            }
        });
        
        //the following statement is commented to hide the addDsaToUSer button in any case
        addDsaToUser.setVisible(false);
//        if (accessControl.getPrincipalName().getUserRole().equalsIgnoreCase("policyExpert"))
//        {
//        	addDsaToUser.setVisible(true);
//        }
//        else
//        {
//        	addDsaToUser.setVisible(false);
//        }
//        
        HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.setSpacing(true);
        topLayout.setWidth("100%");
        topLayout.addComponent(filter);
        topLayout.addComponent(addDsaToUser);
        topLayout.addComponent(newProduct);
        topLayout.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
        topLayout.setExpandRatio(filter, 1);
        topLayout.setStyleName("top-bar");
        return topLayout;
    }

    protected void hideOtherButtons() {
    	
    	form.dsaId.setVisible(false);	    
    	form.dsaDescription.setVisible(false);	
    	form.createTemplate.setVisible(false);	
    	
    	form.dsaTemplates.setVisible(true);
    	form.createDsa.setVisible(true);
    	
    	form.show.setVisible(false);	
    	form.accept.setVisible(false);
    	form.complete.setVisible(false);
    	form.edit.setVisible(false);
    	form.analyse.setVisible(false);
    	form.map.setVisible(false);
    	form.delete.setVisible(false);
    	form.conflict.setVisible(false);
    	form.revoke.setVisible(false);
    	
    	form.cancel.setVisible(true);
    	form.copy.setVisible(false);
    	form.showRaw.setVisible(false);
    
    
		
	}

	private String getMappedRole(String userRole) {
    	String userRoleHPMapping ="";
    	
	    if(userRole.equalsIgnoreCase("legalexpert"))
		{
			userRoleHPMapping ="LegalExpert";
		}
		else if(userRole.equalsIgnoreCase("policyexpert"))
		{
			userRoleHPMapping ="PolicyExpert";
		}
		else
		{
			userRoleHPMapping ="User";
		}
    	return userRoleHPMapping;
  }

	@Override
    public void enter(ViewChangeEvent event) {
    	
    	logger.log(Level.INFO,"***********Entering DSATableView Operations enter with parameters: "+event.getParameters()+"***********");	 
    	
        viewLogic.enter(event.getParameters());
    }

    public void showError(String msg) {
        Notification.show(msg, Type.ERROR_MESSAGE);
    }

    public void showSaveNotification(String msg) {
        Notification.show(msg, Type.TRAY_NOTIFICATION);
    }

    public void setNewProductEnabled(boolean enabled) {
        newProduct.setEnabled(enabled);
    }

    public void clearSelection() {
        grid.getSelectionModel().reset();
    }

    public void selectRow(DSADetails row) {
        ((SelectionModel.Single) grid.getSelectionModel()).select(row);
    }

    public DSADetails getSelectedRow() {
    	logger.log(Level.INFO,"***********getSelectedRow");	 
        return grid.getSelectedRow();
    }

    public void editDsa(DSADetails dsa) {
    	logger.log(Level.INFO,"***********DSADetails");	 
        if (dsa != null) {
            form.addStyleName("visible");
            form.setEnabled(true);
       
        } else {
            form.removeStyleName("visible");
            form.setEnabled(false);
        }
        form.editDsa(dsa);
    }

    public void showDsas(Collection<DSADetails> dsas) {
    	logger.log(Level.INFO,"***********Entering DSATableView showDsas  with dsa size: " +dsas.size()+"***********");	 

        grid.setDsas(dsas);
    }

    public void refreshDsa(DSADetails dsa) {
        grid.refresh(dsa);
        grid.scrollTo(dsa);
    }

    public void removeDsa(DSADetails dsa) {
        grid.remove(dsa);
    }

    public void showRawDsa(DSADetails dsa, String msg) {
        
        StreamResource.StreamSource ss = new StreamResource.StreamSource() {
            private static final long serialVersionUID = 6537911361037391362L;
            InputStream is = new BufferedInputStream(new ByteArrayInputStream(msg.getBytes()));
            @Override
            public InputStream getStream() {
                return is;
            }
        };
        
        //BrowserFrame browser = new BrowserFrame("Browser", new StreamResource(ss, dsa.getDsaId()+".xml"));
        StreamResource res = new StreamResource(ss, dsa.getDsaId()+".xml");
        res.setMIMEType("text/plain"); // to show as plain text; if not set, it will depend on browser, i.e. firefox will show xml tree, chrome will not
        res.setCacheTime(0);
        BrowserFrame browser = new BrowserFrame("Browser", res);
        browser.setSizeFull(); // so it can be resized with the containing window
        Window window = new Window("DSA: "+dsa.getDsaName()+" ("+dsa.getDsaId()+")");
        window.setWidth(800, Sizeable.Unit.PIXELS);
        window.setHeight(600, Sizeable.Unit.PIXELS);
        window.setModal(true);
        window.center();
        window.setContent(browser);
        UI.getCurrent().addWindow(window);
        
    }
    
}
