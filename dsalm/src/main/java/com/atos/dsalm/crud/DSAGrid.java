package com.atos.dsalm.crud;

import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.atos.dsalm.backend.data.Status;
import com.atos.dsalm.backend.data.DSADetails;

import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.MethodProperty;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.converter.StringToEnumConverter;
import com.vaadin.data.util.filter.Or;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.HtmlRenderer;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */

public class DSAGrid extends Grid {
	Logger logger = Logger.getLogger("DSAGrid.class");

    private StringToEnumConverter statusConverter = new StringToEnumConverter() {
        @Override
        public String convertToPresentation(Enum status,java.lang.Class<? extends String> targetType, Locale locale)
            throws Converter.ConversionException {
        	
            String text = super.convertToPresentation(status, targetType,locale);

            String color = getColor(status);
                   
            
            String iconCode = "<span class=\"v-icon\" style=\"font-family: "
                    + FontAwesome.CIRCLE.getFontFamily() + ";color:" + color
                    + "\">&#x"
                    + Integer.toHexString(FontAwesome.CIRCLE.getCodepoint())
                    + ";</span>";

            return iconCode + " " + text;
        }

    };

    public DSAGrid() {
        setSizeFull();

        setSelectionMode(SelectionMode.SINGLE);

        BeanItemContainer<DSADetails> container = new BeanItemContainer<DSADetails>(DSADetails.class);
        setContainerDataSource(container);
        setColumnOrder( "dsaName", "dsaCreator", "dsaVersion","dsaStatus", "type", "createDate" , "startDate", "endDate","dsaId");
        
    	getColumn("dsaId").setHeaderCaption("DSA ID");
    	getColumn("dsaName").setHeaderCaption("DSA Name");
    	getColumn("dsaCreator").setHeaderCaption("DSA Creator");
    	getColumn("dsaVersion").setHeaderCaption("DSA Version");
    //	getColumn("dsaDescription").setHeaderCaption("DSA Description");
    	getColumn("dsaDescription").setHidden(true);
    	getColumn("dsaStatus").setHeaderCaption("DSA Status");
    	getColumn("type").setHidden(true);
    	getColumn("createDate").setHeaderCaption("Create Date");
    	getColumn("startDate").setHeaderCaption("Start Date");
    	getColumn("endDate").setHeaderCaption("Expire Date");




        // Add an traffic light icon in front of availability
        getColumn("dsaStatus").setConverter(statusConverter).setRenderer(new HtmlRenderer());

     

        // Align columns using a style generator and theme rule until #15438
        setCellStyleGenerator(new CellStyleGenerator() {

            @Override
            public String getStyle(CellReference cellReference) {
                if (cellReference.getPropertyId().equals("dsaVersion") ) {
                    return "v-align-center";
                }
                return null;
            }
        });
    }

    protected String getColor(Enum status) {

    	
        
     	 if(status == Status.TEMPLATE)
     	 {
     		 return "#1238cf";
     	 }
     	 else if(status == Status.TMPL_Analysed)
     	 {
     		return "#677110";
     	 }
     	 else if(status == Status.Completed)
     	 {
     		return "#367110";
     	 }
     	 else if(status == Status.Analysed)
     	 {
     		return "#00ffff";
     	 }
     	 else if(status == Status.Mapped)
    	 {
    		return "#00ffff";
    	 }
     	 else if(status == Status.Available)
     	 {
     		return "#1e8369";
     	 }
     	 else if(status == Status.Prepared)
     	 {
     		return "#52b2b5";
     	 }
     	 else if(status == Status.Available_Approved)
     	 {
     		return "#d1b771";
     	 }
     	 else if(status == Status.Customised)
     	 {
     		return "#d6e822";
     	 }
     	 else if(status == Status.Conflict)
     	 {
     		return "#B21F0E";
     	 }
     	else if(status == Status.NYA || status == Status.Exp)
    	 {
    		return "#B21F0E";
    	 }

      return "#B21F0E";
  	
	}

	/**
     * Filter the grid based on a search string that is searched for in the
     * product name, availability and category columns.
     *
     * @param filterString
     *            string to look for
     */
    public void setFilter(String filterString) {
    	logger.log(Level.INFO,"***********Entering DSAGrid setFilter with filterString: " +filterString+"***********");	 

        getContainer().removeAllContainerFilters();
        if (filterString.length() > 0) {
            SimpleStringFilter dsaIdFilter = new SimpleStringFilter(
                    "dsaId", filterString, true, false);
            SimpleStringFilter dsaNameFilter = new SimpleStringFilter(
                    "dsaName", filterString, true, false);
            SimpleStringFilter dsaStatusFilter = new SimpleStringFilter(
                    "dsaStatus", filterString, true, false);
            SimpleStringFilter dsaCreatorFilter = new SimpleStringFilter(
                    "dsaCreator", filterString, true, false);
            SimpleStringFilter dsaCreateDateFilter = new SimpleStringFilter(
                    "createDate", filterString, true, false);
            getContainer().addContainerFilter(
                    new Or(dsaIdFilter, dsaNameFilter, dsaStatusFilter,dsaCreatorFilter, dsaCreateDateFilter));
        }

    }

    private BeanItemContainer<DSADetails> getContainer() {
        return (BeanItemContainer<DSADetails>) super.getContainerDataSource();
    }

    @Override
    public DSADetails getSelectedRow() throws IllegalStateException {
        return (DSADetails) super.getSelectedRow();
    }

    public void setDsas(Collection<DSADetails> dsas) {
    	logger.log(Level.INFO,"***********Entering DSAGrid setDsas with dsa size: " +dsas.size()+"***********");	 

        getContainer().removeAllItems();
        getContainer().addAll(dsas);

    /*	for (Iterator iterator = dsas.iterator(); iterator.hasNext();) {
    			   		
    			DSADetails dsaDetails = (DSADetails) iterator.next();
    			logger.log(Level.INFO,"***********" +dsaDetails.getDsaCreator()+" "+dsaDetails.getDsaDescription()+" "+dsaDetails.getDsaId()+" "+dsaDetails.getDsaStatus()+" "+
    			dsaDetails.getDsaName()+" "+dsaDetails.getDsaVersion()+" "+dsaDetails.getType()+"***********");	
        	  //  getContainer().addBean(dsaDetails);
    	} */
        
    }   
    

    public void refresh(DSADetails dsa) {
    	
    	logger.log(Level.INFO,"***********Entering DSAGrid refresh with dsa : " +dsa.getDsaId()+"***********");	 
        // We avoid updating the whole table through the backend here so we can
        // get a partial update for the grid
        BeanItem<DSADetails> item = getContainer().getItem(dsa);
        if (item != null) {
            // Updated product
            MethodProperty p = (MethodProperty) item.getItemProperty("id");
            p.fireValueChange();
        } else {
            // New product
            getContainer().addBean(dsa);
        }
    }

    public void remove(DSADetails dsa) {
    	logger.log(Level.INFO,"***********Entering DSAGrid remove with dsa : " +dsa.getDsaId()+"***********");	 

        getContainer().removeItem(dsa);
    }
}
