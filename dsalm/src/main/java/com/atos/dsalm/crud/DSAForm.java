package com.atos.dsalm.crud;

import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.vaadin.dialogs.ConfirmDialog;

import com.atos.dsalm.Configurator;
import com.atos.dsalm.SpringContextBridge;
import com.atos.dsalm.authentication.AccessControl;
import com.atos.dsalm.authentication.BasicAccessControl;
import com.atos.dsalm.backend.DataService;
import com.atos.dsalm.backend.data.DSADetails;
import com.atos.dsalm.backend.data.DSATempDetails;
import com.atos.dsalm.backend.data.ResultObject;
import com.atos.dsalm.backend.data.Status;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification.Type;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */

public class DSAForm extends DSAFormDesign {

    // get configurator for properties
    private Configurator cfg = SpringContextBridge.services().getConfigurator();
    private String dsaAtUrl = cfg.getProperty("dsalm.dsaat.url");
    private String dsaEditorFeUrl = cfg.getProperty("dsalm.dsaeditorfe.url");
    
    private DSATableLogic viewLogic;
    private BeanFieldGroup<DSADetails> fieldGroup;
	private AccessControl accessControl = new BasicAccessControl();
	Logger logger = Logger.getLogger("DSAForm.class");
    public DSAForm(DSATableLogic dsaTableLogic) {
        super();
        addStyleName("product-form");
        viewLogic = dsaTableLogic;

        Collection<DSATempDetails> myDSATemplates =  DataService.get().getDSATempDetails(accessControl.getPrincipalName().getUserId());

        for (Iterator iterator = myDSATemplates.iterator(); iterator.hasNext();) {
			DSATempDetails dsaTempDetails = (DSATempDetails) iterator.next();

			dsaTemplates.addItem(dsaTempDetails.getDsaId());
			dsaTemplates.setItemCaption(dsaTempDetails.getDsaId(), dsaTempDetails.getDsaName());

		}



        dsaTemplates.setInputPrompt("Select and Click Create DSA");
        dsaTemplates.setFilteringMode(FilteringMode.CONTAINS);



        fieldGroup = new BeanFieldGroup<DSADetails>(DSADetails.class);
        fieldGroup.bindMemberFields(this);

        createTemplate.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {

            	String userRoleHPMapping =getMappedRole(accessControl.getPrincipalName().getUserRole());
            	String myUser=accessControl.getPrincipalName().getUserId();
            	String mySelectedDSA = "";
            	getUI().getPage().setLocation(dsaAtUrl+"/?issuer="+userRoleHPMapping+"&userID="+myUser+"&dsaId=&action=Create&target="+dsaEditorFeUrl);

            }
        });

        createDsa.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {

            	if (dsaTemplates.getValue()!= null)
            	{


            		ResultObject result = DataService.get().copyDsaTemplate(dsaTemplates.getValue().toString(),accessControl.getPrincipalName().getUserId());

            		if(result.getStatus())
            		{
            			String userRoleHPMapping =getMappedRole(accessControl.getPrincipalName().getUserRole());
                    	String myUser=accessControl.getPrincipalName().getUserId();
                    	String myDsaId = result.getMessage().trim();
                    	getUI().getPage().setLocation(dsaAtUrl+"/?issuer="+userRoleHPMapping+"&userID="+myUser+"&dsaId="+myDsaId+".xml"+"&action=Update&target="+dsaEditorFeUrl);
            		}
            		else
            		{
            			Notification.show("Failed to create DSA", Type.TRAY_NOTIFICATION);
            		}
            	}
            	else
            	{
            		Notification.show("Please Select A DSA Template", Type.TRAY_NOTIFICATION);
            	}

            }
        });

        show.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	String userRoleHPMapping = getMappedRole(accessControl.getPrincipalName().getUserRole());
            	String myUser= accessControl.getPrincipalName().getUserId();
            	String mySelectedDSA = fieldGroup.getItemDataSource().getBean().getDsaId();
            	getUI().getPage().setLocation(dsaAtUrl+"/?issuer="+userRoleHPMapping+"&userID="+myUser+"&dsaId="+mySelectedDSA+".xml"+"&action=Read&target="+dsaEditorFeUrl);

            }
        });

        accept.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {

            	ResultObject result = DataService.get().updateDsaStatus(fieldGroup.getItemDataSource().getBean().getDsaId(),"AVAILABLE+APPROVED");
            	if(result.getStatus())
            	{
        			Notification.show("DSA accepted", Type.TRAY_NOTIFICATION);
        			getUI().getPage().reload();
        		}
            	else
            	{
        			Notification.show("Failed to accept DSA", Type.TRAY_NOTIFICATION);
        			 removeStyleName("visible");
        	         setEnabled(false);
        		}

            }
        });

        edit.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {

            	if(accessControl.getPrincipalName().getUserRole().equalsIgnoreCase("legalexpert"))
            	{
            		/*if(fieldGroup.getItemDataSource().getBean().getDsaStatus() == Status.NYA || fieldGroup.getItemDataSource().getBean().getDsaStatus() == Status.Exp )
            		{
            		} */
        			ResultObject result = DataService.get().updateDsaStatusFile(fieldGroup.getItemDataSource().getBean().getDsaId(),"TEMPLATE");


            	}
                else if (!(fieldGroup.getItemDataSource().getBean().getDsaStatus() == Status.Completed  || fieldGroup.getItemDataSource().getBean().getDsaStatus() == Status.Prepared) )
            	{
            		ResultObject result = DataService.get().updateDsaStatusFile(fieldGroup.getItemDataSource().getBean().getDsaId(),"CUSTOMISED");
            	}

            	String userRoleHPMapping =getMappedRole(accessControl.getPrincipalName().getUserRole());
            	String myUser= accessControl.getPrincipalName().getUserId();
            	String mySelectedDSA = fieldGroup.getItemDataSource().getBean().getDsaId();
            	getUI().getPage().setLocation(dsaAtUrl+"/?issuer="+userRoleHPMapping+"&userID="+myUser+"&dsaId="+mySelectedDSA+".xml"+"&action=Update&target="+dsaEditorFeUrl);

            }
        });

        complete.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {

             	String userRoleHPMapping =getMappedRole(accessControl.getPrincipalName().getUserRole());
            	String myUser= accessControl.getPrincipalName().getUserId();
            	String mySelectedDSA = fieldGroup.getItemDataSource().getBean().getDsaId();
            	getUI().getPage().setLocation(dsaAtUrl+"/?issuer="+userRoleHPMapping+"&userID="+myUser+"&dsaId="+mySelectedDSA+".xml"+"&action=Update&target="+dsaEditorFeUrl);


            /*	ResultObject result = DataService.get().updateDsaStatus(fieldGroup.getItemDataSource().getBean().getDsaId(),"AVAILABLE");
            	if(result.getStatus())
            	{
        			Notification.show("DSA completed", Type.TRAY_NOTIFICATION);
        			getUI().getPage().reload();
        		}
            	else
            	{
        			Notification.show("Failed to complete DSA", Type.TRAY_NOTIFICATION);
        			 removeStyleName("visible");
        	         setEnabled(false);
        		}

             */
            }
        });

        analyse.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {



            	ResultObject result = DataService.get().analyseDsa(fieldGroup.getItemDataSource().getBean().getDsaId());
            	if(result.getStatus())
            	{
        			Notification.show("DSA Analysed", Type.TRAY_NOTIFICATION);
        			getUI().getPage().reload();
        		}
            	else
            	{
        			Notification.show("Failed to analysed DSA", Type.TRAY_NOTIFICATION);
        			 removeStyleName("visible");
        	         setEnabled(false);
        		}


            }
        });

        map.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {


            	// ResultObject result = DataService.get().updateDsaStatus(fieldGroup.getItemDataSource().getBean().getDsaId(),"AVAILABLE");

            	ResultObject result = DataService.get().mapDsa(fieldGroup.getItemDataSource().getBean().getDsaId());
            	if(result.getStatus())
            	{
        			Notification.show("DSA Mapped", Type.TRAY_NOTIFICATION);
        			getUI().getPage().reload();
        		}
            	else
            	{
        			Notification.show("Failed to Map DSA", Type.TRAY_NOTIFICATION);
        			 removeStyleName("visible");
        	         setEnabled(false);
        		}



            }
        });


        revoke.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {

            	ConfirmDialog.show(getUI(), "Please Confirm:", "Are you really sure?",
            	        "Yes", "No, Cancel", new ConfirmDialog.Listener() {

            	            public void onClose(ConfirmDialog dialog) {
            	                if (dialog.isConfirmed()) {
            	                    // Confirmed to continue
            	                	ResultObject result = DataService.get().revokeDsa(fieldGroup.getItemDataSource().getBean().getDsaId());
            	                	if(result.getStatus())
            	                	{
            	            			Notification.show("DSA Revoked", Type.TRAY_NOTIFICATION);
            	            			getUI().getPage().reload();
            	            		}
            	                	else
            	                	{
            	            			Notification.show("Failed to Revoke DSA", Type.TRAY_NOTIFICATION);
            	            			removeStyleName("visible");
            	            	        setEnabled(false);
            	            		}
            	                } else {
            	                    // User did not confirm

            	                }
            	            }
            	        });




            }
        });

        cancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                viewLogic.cancelProduct();
            }
        });

        copy.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                
                ConfirmDialog.show(getUI(), "Please Confirm:", "Copying DSA: "+fieldGroup.getItemDataSource().getBean().getDsaName()+"\n\nAre you really sure?",
                        "Yes", "No, Cancel", new ConfirmDialog.Listener() {

                            public void onClose(ConfirmDialog dialog) {
                                if (dialog.isConfirmed()) {
                                      // Confirmed to continue
                                      ResultObject result = DataService.get().copyDsaTemplate(fieldGroup.getItemDataSource().getBean().getDsaId(),accessControl.getPrincipalName().getUserId());
                                      if(result.getStatus())
                                      {
                                          Notification.show("DSA Copied", Type.TRAY_NOTIFICATION);
                                          getUI().getPage().reload();
                                      }
                                      else
                                      {
                                          Notification.show("Failed to copy DSA", Type.TRAY_NOTIFICATION);
                                           removeStyleName("visible");
                                           setEnabled(false);
                                      }
                                } else {
                                    // User did not confirm

                                }
                            }
                        });
                

                
            }
        });
        
        delete.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {



            	ConfirmDialog.show(getUI(), "Please Confirm:", "Are you really sure?",
            	        "Yes", "No, Cancel", new ConfirmDialog.Listener() {

            	            public void onClose(ConfirmDialog dialog) {
            	                if (dialog.isConfirmed()) {
            	                	  // Confirmed to continue
            	                 	DSADetails dsa = fieldGroup.getItemDataSource().getBean();
            	                    viewLogic.deleteDsa(dsa);
            	                } else {
            	                    // User did not confirm

            	                }
            	            }
            	        });


            }
        });

        conflict.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {

            	 viewLogic.showConflicts(fieldGroup.getItemDataSource().getBean().getDsaId());
            }
        });
        
        showRaw.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                 viewLogic.showRawDsa(fieldGroup.getItemDataSource().getBean().getDsaId());
            }
        });
        
    }

    public void editDsa(DSADetails dsas) {
    	logger.log(Level.INFO,"***********editDsa");
    	if(dsas != null)
    	{
    		setEnableDisableButtouns(dsas.getDsaStatus(),accessControl.getPrincipalName().getUserRole());
    	}
        if (dsas == null) {
        	dsas = new DSADetails();
        }
        fieldGroup.setItemDataSource(new BeanItem<DSADetails>(dsas));

               // Scroll to the top
        // As this is not a Panel, using JavaScript
        String scrollScript = "window.document.getElementById('" + getId()
                + "').scrollTop = 0;";
        Page.getCurrent().getJavaScript().execute(scrollScript);
    }

	private void setEnableDisableButtouns(Status dsaStatus, String userRole) {

		if(userRole.equalsIgnoreCase("legalexpert"))
		{

		    accept.setVisible(false);
		    complete.setVisible(false);
		    map.setVisible(false);
		    accept.setVisible(false);
		    dsaTemplates.setVisible(false);
		    createDsa.setVisible(false);
		    // will be deleted if not effecting other components
		    createTemplate.setVisible(false);
		    conflict.setVisible(false);

		    dsaId.setVisible(true);
		    dsaDescription.setVisible(true);
		    show.setVisible(true);
		    edit.setVisible(true);
		    analyse.setVisible(true);
		    delete.setVisible(true);
		    cancel.setVisible(true);
		    revoke.setVisible(true);
		    copy.setVisible(true);
		    showRaw.setVisible(true);
		    if(dsaStatus == Status.TMPL_Analysed)
		    {
		    	 analyse.setVisible(false);
		    }
		    if(dsaStatus == Status.Conflict)
		    {
		    	 conflict.setVisible(true);
		    }

		    if(dsaStatus == Status.Analysing)
		    {
		    	hideOtherButtons();
		    	 show.setVisible(true);
		    	 cancel.setVisible(true);
		    	 copy.setVisible(true);
		    	 showRaw.setVisible(true);
		    }
		    if(dsaStatus == Status.Revoke )
		    {
		    	hideOtherButtons();
		    	 delete.setVisible(true);
		    	 show.setVisible(true);
		    	 cancel.setVisible(true);
		    	 copy.setVisible(true);
		    	 showRaw.setVisible(true);
		    }

		}
		else if(userRole.equalsIgnoreCase("policyExpert"))
		{
			createTemplate.setVisible(false);
		    accept.setVisible(false);
		    complete.setVisible(false);
		    edit.setVisible(false);
		    analyse.setVisible(false);
		    map.setVisible(false);
		    accept.setVisible(false);
		    // Hide the visible items when user clicks on the grid requested by HP (Carmela)
		    dsaTemplates.setVisible(false);
		    createDsa.setVisible(false);
		    conflict.setVisible(false);

		    dsaId.setVisible(true);
		    dsaDescription.setVisible(true);
		    show.setVisible(true);

		    delete.setVisible(true);
		    cancel.setVisible(true);
		    revoke.setVisible(true);
		    copy.setVisible(true);
		    showRaw.setVisible(true);
		    
		    if(dsaStatus == Status.Revoke )
		    {
		    	revoke.setVisible(false);
		    }

		    if(dsaStatus == Status.Available_Approved || dsaStatus == Status.Available || dsaStatus == Status.Customised  || dsaStatus == Status.TEMPLATE || dsaStatus == Status.NYA || dsaStatus == Status.Exp )
		    {
		    	 edit.setVisible(true);
		    }
		    if(dsaStatus == Status.Analysed)
		    {
		    	  map.setVisible(false);
		    	  edit.setVisible(true);
		    }
		    if(dsaStatus == Status.Completed)
		    {
		    	  map.setVisible(true);
		    }
		    if( dsaStatus == Status.Customised )
		    {
		        analyse.setVisible(true);
		    }

		    if(dsaStatus == Status.Conflict)
		    {
		    	 map.setVisible(false);
		    	 conflict.setVisible(true);
		    	 edit.setVisible(true);
		    }

		    if(dsaStatus == Status.Analysing)
		    {
		    	hideOtherButtons();
		    	 show.setVisible(true);
		    	 cancel.setVisible(true);
		    	 copy.setVisible(true);
		    	 showRaw.setVisible(true);
		    }
		}
		else if(userRole.equalsIgnoreCase("endUser"))
		{
			createTemplate.setVisible(false);
			createDsa.setVisible(false);
		    dsaTemplates.setVisible(false);
		    accept.setVisible(false);
		    complete.setVisible(false);
		    edit.setVisible(false);
		    analyse.setVisible(false);
		    map.setVisible(false);
		    delete.setVisible(false);
		    conflict.setVisible(false);

		    dsaId.setVisible(true);
		    dsaDescription.setVisible(true);
		    show.setVisible(true);
		    cancel.setVisible(true);
		    revoke.setVisible(false);
		    copy.setVisible(true);
		    showRaw.setVisible(true);
		    
		    if(dsaStatus == Status.Available )
		    {
		    	 accept.setVisible(true);
		    }

		    if(dsaStatus == Status.Prepared)
		    {
		    	 complete.setVisible(true);
		    }
		    if(dsaStatus == Status.Analysing)
		    {
		    	hideOtherButtons();
		    	 show.setVisible(true);
		    	 cancel.setVisible(true);
		    	 copy.setVisible(true);
		    	 showRaw.setVisible(true);
		    }

		}
		else
		{
			// print error handle error
		}

		//always hide analyse
		 analyse.setVisible(false);
	}


	private String getMappedRole(String userRole) {
    	String userRoleHPMapping ="";

	    if(userRole.equalsIgnoreCase("legalexpert"))
		{
			userRoleHPMapping ="LegalExpert";
		}
		else if(userRole.equalsIgnoreCase("policyexpert"))
		{
			userRoleHPMapping ="PolicyExpert";
		}
		else
		{
			userRoleHPMapping ="User";
		}
    	return userRoleHPMapping;
  }
	private void hideOtherButtons() {

    	dsaId.setVisible(true);
    	dsaDescription.setVisible(true);
    	createTemplate.setVisible(false);

    	dsaTemplates.setVisible(false);
    	createDsa.setVisible(false);

    	show.setVisible(false);
    	accept.setVisible(false);
    	complete.setVisible(false);
    	edit.setVisible(false);
    	analyse.setVisible(false);
    	map.setVisible(false);
    	delete.setVisible(false);
    	conflict.setVisible(false);
    	revoke.setVisible(false);
    	copy.setVisible(false);
    	showRaw.setVisible(false);


	}


}
