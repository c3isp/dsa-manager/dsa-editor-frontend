package com.atos.dsalm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

//@ServletComponentScan //TODO: disable loading of webfiler AddSecurityResponseHeaderFilter because of integration with Pilots cannot handle X-Frame-Options
@SpringBootApplication
public class DsaLmApplication extends SpringBootServletInitializer {

    //private static final Logger log = LoggerFactory.getLogger(DsaLmApplication.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(DsaLmApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(DsaLmApplication.class, args);
    }
}
