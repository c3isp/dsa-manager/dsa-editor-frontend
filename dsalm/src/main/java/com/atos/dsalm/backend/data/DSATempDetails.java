package com.atos.dsalm.backend.data;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;


@SuppressWarnings("serial")
public class DSATempDetails extends DSADetails {
	  private String dsaName;
	  private String dsaId;
	  
	  public DSATempDetails() {
	  }
	  
	  public DSATempDetails(String dsaId, String dsaName) {
			
		this.setDsaId(dsaId);
	    this.setDsaName(dsaName);

	  
	    
	  }
	  


		public String getDsaName() {
			return dsaName;
		}

		public void setDsaName(String dsaName) {
			this.dsaName = dsaName;
		}
		public String getDsaId() {
			return dsaId;
		}

		public void setDsaId(String dsaId) {
			this.dsaId = dsaId;
		}
}
