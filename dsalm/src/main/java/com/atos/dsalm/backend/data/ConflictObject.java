package com.atos.dsalm.backend.data;

public class ConflictObject {
	
	 String auth; 
	 String prohib;
	 String obli;
	 String context;
	
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	public String getProhib() {
		return prohib;
	}
	public void setProhib(String prohib) {
		this.prohib = prohib;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public String getObli() {
		return obli;
	}
	public void setObli(String obli) {
		this.obli = obli;
	}

}
