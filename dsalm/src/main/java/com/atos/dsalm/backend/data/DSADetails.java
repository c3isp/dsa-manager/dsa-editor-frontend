package com.atos.dsalm.backend.data;

import java.io.Serializable;
import javax.validation.constraints.NotNull;


public class DSADetails implements Serializable {
	  
	  @NotNull
	  private String dsaId = "DSA-1";
	  @NotNull
	  private String dsaName = "DSA Name";
	  @NotNull
	  private String dsaCreator = "DSA User";
	  @NotNull
	  private String dsaVersion= "DSA Version 1";
	  
	  @NotNull
	  private String createDate= "2016-05-09 02:00:00";
	  @NotNull
	  private String startDate= "2016-02-17 01:00:00";
	  @NotNull
	  private String endDate= "2016-08-10 02:00:00";
	  
	  private String dsaDescription = "No Description Provided";
	  @NotNull
	  private Status dsaStatus = Status.TEMPLATE;
	  @NotNull
	  private int type = 0 ;
	  
	public String getDsaId() {
		return dsaId;
	}
	public void setDsaId(String dsaId) {
		this.dsaId = dsaId;
	}
	public String getDsaName() {
		return dsaName;
	}
	public void setDsaName(String dsaName) {
		this.dsaName = dsaName;
	}
	public String getDsaCreator() {
		return dsaCreator;
	}
	public void setDsaCreator(String dsaCreator) {
		this.dsaCreator = dsaCreator;
	}
	public String getDsaVersion() {
		return dsaVersion;
	}
	public void setDsaVersion(String dsaVersion) {
		this.dsaVersion = dsaVersion;
	}
	public String getDsaDescription() {
		return dsaDescription;
	}
	public void setDsaDescription(String dsaDescription) {
		this.dsaDescription = dsaDescription;
	}
	public Status getDsaStatus() {
		return dsaStatus;
	}
	public void setDsaStatus(Status dsaStatus) {
		this.dsaStatus = dsaStatus;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	  
	  
	  	

}
