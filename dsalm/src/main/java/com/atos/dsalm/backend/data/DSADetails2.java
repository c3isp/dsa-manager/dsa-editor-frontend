package com.atos.dsalm.backend.data;
import java.io.Serializable;

@SuppressWarnings("serial")
public class DSADetails2 implements Serializable {
	 /*test*/
	  private String dsaName;
	  private String dsaCreator;
	  private String dsaVersion;
	  private String dsaDescription;
	  private String dsaStatus;
	  private int type;
	  
	  private String createDate;
	  private String startDate;
	  private String endDate;
	  
	  private String dsaId;
	  
	  public DSADetails2() {
	  }

	  public DSADetails2(String dsaId, String dsaName, String dsaCreator, String dsaVersion, String dsaDescription, String dsaStatus, int dsaType) {
		
		this.setDsaId(dsaId);
	    this.setDsaName(dsaName);
	    this.setDsaCreator(dsaCreator);
	    this.setDsaVersion(dsaVersion);
	    this.setDsaDescription(dsaDescription);
	    this.setDsaStatus(dsaStatus);
	    this.setType(dsaType);
	  
	    
	  }

	public String getDsaName() {
		return dsaName;
	}

	public void setDsaName(String dsaName) {
		this.dsaName = dsaName;
	}

	public String getDsaCreator() {
		return dsaCreator;
	}

	public void setDsaCreator(String dsaCreator) {
		this.dsaCreator = dsaCreator;
	}

	public String getDsaVersion() {
		return dsaVersion;
	}

	public void setDsaVersion(String dsaVersion) {
		this.dsaVersion = dsaVersion;
	}

	public String getDsaDescription() {
		return dsaDescription;
	}

	public void setDsaDescription(String dsaDescription) {
		this.dsaDescription = dsaDescription;
	}

	public String getDsaStatus() {
		return dsaStatus;
	}

	public void setDsaStatus(String dsaStatus) {
		this.dsaStatus = dsaStatus;
	}


	public int getType() {
		return type;
	}


	public void setType(int type) {
		this.type = type;
	}

	public String getDsaId() {
		return dsaId;
	}

	public void setDsaId(String dsaId) {
		this.dsaId = dsaId;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}




}
