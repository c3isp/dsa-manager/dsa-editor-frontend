package com.atos.dsalm.backend.data;


public enum Status {
    TEMPLATE("Template"), TMPL_Analysed("Analysed Template"), Completed("Completed"),Analysed("Analysed"),Available("Available"),Analysing("DSA Is Being Analysed"),
    Prepared("Prepared"),Available_Approved("Available and Approved"),Customised("Customised"),Conflict("DSA  Has Conflicts"),Mapped("Mapped"),NYA("DSA Start Date Not Valid Yet"),Exp("DSA Is Expired"), Revoke("DSA Is Revoked"),Undefined("Status Name Is Undefined");

    private final String name;

    private Status(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
    
    public static Status get(String enumType) {
        switch(enumType.toUpperCase()) {
            case  "TEMPLATE": return TEMPLATE;
            case  "TMPL_ANALYSED": return TMPL_Analysed;
            case "COMPLETED": return Completed;
            case "ANALYSED": return Analysed;
            case "AVAILABLE": return Available;
            case "PREPARED": return Prepared;
            case "AVAILABLE+APPROVED": return Available_Approved;
            case "CUSTOMIZED": return Customised;
            case "CUSTOMISED": return Customised;
            case "CONFLICT": return Conflict;
            case "MAPPED": return Mapped;
            case "NOT_YET_AVAILABLE": return NYA;
            case "EXPIRED": return Exp;
            case "REVOKED": return Revoke;
            case "ANALYSING": return Analysing;
         
        }
        return Undefined;
    }
}
