package com.atos.dsalm.backend.data;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;


public class ResultObject {

	 private Boolean status;
	 private String message;

	public String getMessage() {
		return message;
	}
	public Boolean getStatus() {
		return status;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}



}
