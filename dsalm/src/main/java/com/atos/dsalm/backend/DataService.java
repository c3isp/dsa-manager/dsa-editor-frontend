package com.atos.dsalm.backend;

import java.util.Collection;

import com.atos.dsalm.backend.data.DSADetails;
import com.atos.dsalm.backend.data.DSATempDetails;
import com.atos.dsalm.backend.data.ResultObject;
import com.atos.dsalm.backend.dsaservice.DSADataService;
import com.atos.dsalm.objects.UserDetails;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;

/**
 * Back-end service interface for retrieving and updating product data.
 */

public abstract class DataService {

    public abstract Collection<DSADetails> getAllDsas(String userId);

	public abstract Collection<DSATempDetails>  getDSATempDetails(String userId) ;

	public abstract ResultObject addDsaToUser(String dsaId, String userId) ;

    public abstract ResultObject deleteDsa(String dsaId);

    public abstract DSADetails getDsaById(String dsaId , String userId);

	public abstract ResultObject copyDsaTemplate(String string, String userId) ;
	
	public abstract ResultObject updateDsaStatus(String dsaId,String dsaStatus) ;
	
	public abstract ResultObject updateDsaStatusFile(String dsaId,String dsaStatus) ;
	
	public abstract ResultObject mapDsa(String dsaId) ;
	
	public abstract ResultObject revokeDsa(String dsaId) ;
	
	public abstract ResultObject analyseDsa(String dsaId) ;
	
	public abstract ResultObject getDsaConflicts(String dsaId);
	
	public abstract ResultObject getShowRaw(String dsaId);
	
    public static DataService get() {
        return DSADataService.getInstance();
    }

	public abstract Collection<UserDetails> getAllEndUsers();






}
