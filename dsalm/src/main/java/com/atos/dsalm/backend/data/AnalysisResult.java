package com.atos.dsalm.backend.data;

import java.util.List;
public class AnalysisResult {
	
	 String status;
	 String  dsaid;
	 List<ConflictObject> policy;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDsaid() {
		return dsaid;
	}
	public void setDsaid(String dsaid) {
		this.dsaid = dsaid;
	}
	public List<ConflictObject> getPolicy() {
		return policy;
	}
	public void setPolicy(List<ConflictObject> policy) {
		this.policy = policy;
	}






}
