package com.atos.dsalm.backend.dsaservice;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.atos.dsalm.backend.data.DSADetails;
import com.atos.dsalm.backend.data.DSADetails2;
import com.atos.dsalm.backend.data.DSATempDetails;
import com.atos.dsalm.backend.data.ResultObject;
import com.atos.dsalm.backend.data.Status;
import com.atos.dsalm.objects.UserDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.atos.dsalm.Configurator;
import com.atos.dsalm.SpringContextBridge;
import com.atos.dsalm.backend.DataService;

/**
 * Mock data model. This implementation has very simplistic locking and does not
 * notify users of modifications.
 */
public class DSADataService extends DataService {
	static Logger logger = Logger.getLogger("BasicAccessControl.class");

	//String dbURL = "jdbc:mysql://localhost:3306/dsarepo"; //TODO: move to config file
    //String username = "dsarepouser"; //TODO: move to config file
    //String password = "MyPWDusr18!!"; //TODO: move to config file

    //private static String url ="https://localhost:8443/DSAAPI"; //local, use this when commiting code
    //private static String url ="https://dsamgrc3isp.iit.cnr.it:8443/DSAAPI"; //remote, use this for local test
    //private static String url ="https://dsamgrc3isp.rp.bt.com:8443/DSAAPI"; //remote, use this for local test
    // DSA API url
    private String url;
    private String dbURL;
    private String username;
    private String password;
    
    // get configurator for properties
    private Configurator cfg = SpringContextBridge.services().getConfigurator();
    
    private static DSADataService INSTANCE;

    private DSADataService() {
        logger.log(Level.INFO, "DSA Data Service constructor - cfg="+cfg);
        url      = cfg.getProperty("dsalm.dsaapi.url");
        dbURL    = cfg.getProperty("dsalm.db.url");
        username = cfg.getProperty("dsalm.db.username");
        password = cfg.getProperty("dsalm.db.password");
        
    }

    @Autowired
    private ObjectMapper mapper = new ObjectMapper();

	private List<DSADetails> getDsaDetails(String userId) {

	   	logger.log(Level.INFO,"Entering getDsaDetails ");
	   	RestTemplate template = new RestTemplate();
		Map<String, String> parts = new HashMap<String, String>();

		parts.put("userid", userId);

	    //SSLCertificateValidation.disable();
		List<DSADetails2> myDsas = new ArrayList<DSADetails2>();

		try {
			ResponseEntity<DSADetails2[]> response = template.getForEntity(url+"/getdsadetails/{userid}", DSADetails2[].class, parts);

			  if (response.getStatusCode() == HttpStatus.OK)
			  {
				  DSADetails2[] myArr =  response.getBody();
				  myDsas = Arrays.asList(myArr);
			  }


		} catch (RestClientException e) {

			e.printStackTrace();
			logger.log(Level.SEVERE,"getDsaDetails RestClientException: "+e.getMessage());
		}
		catch (Exception e) {

			e.printStackTrace();
			logger.log(Level.SEVERE,"getDsaDetails RestClientException: "+e.getMessage());
		}

    	List<DSADetails> dsas  = new ArrayList<DSADetails>();


        for (int i = 0; i < myDsas.size(); i++) {

          	DSADetails dsa = new DSADetails();

    	    	dsa.setDsaId(myDsas.get(i).getDsaId());
    	    	dsa.setDsaName(myDsas.get(i).getDsaName());
    	    	dsa.setDsaCreator(myDsas.get(i).getDsaCreator());
    	    	dsa.setDsaVersion(myDsas.get(i).getDsaVersion());
    	    	dsa.setDsaDescription(myDsas.get(i).getDsaDescription());
    	    	Status myDsaStatus = Status.get(myDsas.get(i).getDsaStatus());
    	    	dsa.setDsaStatus(myDsaStatus);
    	    	dsa.setType(myDsas.get(i).getType());
    	    	dsa.setCreateDate(myDsas.get(i).getCreateDate());
    	    	dsa.setStartDate(myDsas.get(i).getStartDate());
    	    	dsa.setEndDate(myDsas.get(i).getEndDate());

    	    	dsas.add(dsa);

        }



	    return dsas;

    }
    private List<DSATempDetails> getTempDetails(String userId) {

	   	logger.log(Level.INFO,"Entering getDsaDetails");
	   	RestTemplate template = new RestTemplate();
		Map<String, String> parts = new HashMap<String, String>();

		parts.put("userid", userId);

	    //SSLCertificateValidation.disable();
		List<DSATempDetails> dsaTemps = new ArrayList<DSATempDetails>();

		try {
			ResponseEntity<DSATempDetails[]> response = template.getForEntity(url+"/getdsatempdetails/{userid}", DSATempDetails[].class, parts);

			  if (response.getStatusCode() == HttpStatus.OK)
			  {
				  DSATempDetails[] myArr =  response.getBody();
				  dsaTemps = Arrays.asList(myArr);
			  }


		} catch (RestClientException e) {

			e.printStackTrace();
			logger.log(Level.SEVERE,"getDsaDetails RestClientException: "+e.getMessage());
		}
		catch (Exception e) {

			e.printStackTrace();
			logger.log(Level.SEVERE,"getDsaDetails RestClientException: "+e.getMessage());
		}

  return dsaTemps;
}

    private List<UserDetails> getAllEndUserDetails() {

	   	logger.log(Level.INFO,"Entering getAllEndUserDetails");
	   	RestTemplate template = new RestTemplate();


		List<UserDetails> endUsers = new ArrayList<UserDetails>();

	    	Connection conn=null;
	    	logger.log(Level.INFO,"***********Entering getAllEndUserDetails Operations ");

			try {

				Class.forName("com.mysql.cj.jdbc.Driver");

			   	 conn = DriverManager.getConnection(dbURL, username, password);

	        	String sql = "SELECT userName, userId, userRole FROM dsarepo.user ";

	        	PreparedStatement statement = conn.prepareStatement(sql);


	        	ResultSet result = statement.executeQuery();

	        	while (result.next()){
	        		UserDetails user = new UserDetails();
	        		user.setUserId(result.getString(2).trim());
	        		user.setUserName(result.getString(1).trim());
	        		user.setUserRole(result.getString(3).trim());
	        		endUsers.add(user);
	        	}


			} catch (Exception e) {

		    	logger.log(Level.SEVERE,"***********Entering getAllEndUserDetails Operations "+e.getMessage());
		    	e.printStackTrace();

			}

  return endUsers;
}
	public synchronized static DataService getInstance() {
	   	 logger.log(Level.INFO,"Entering getInstance ");

        if (INSTANCE == null) {
            INSTANCE = new DSADataService();
        }
        return INSTANCE;
    }

	@Override
	public List<UserDetails> getAllEndUsers() {
	 	logger.log(Level.INFO,"Entering getAllEndUsers ");
	   	return getAllEndUserDetails();
	}

	@Override
    public synchronized List<DSADetails> getAllDsas(String userId) {
   	logger.log(Level.INFO,"Entering getAllDsas ");
   	return getDsaDetails(userId);
    }

    @Override
    public synchronized List<DSATempDetails> getDSATempDetails(String userId) {
   	 logger.log(Level.INFO,"Entering getDSATempDetails ");
   	 return getTempDetails(userId);
    }



    @Override
    public synchronized DSADetails getDsaById(String dsaId, String userId) {
    	 logger.log(Level.INFO,"Entering getDsaById ");
    	 List<DSADetails> dsas = getDsaDetails(userId);

        for (int i = 0; i < dsas.size(); i++) {
            //if (dsas.get(i).getDsaId() == dsaId) { // string compare is done via equals not ==
            if (dsas.get(i).getDsaId().equals(dsaId)) {
                return dsas.get(i);
            }
        }
        return null;
    }

    @Override
    public synchronized ResultObject deleteDsa(String dsaId) {
   	 logger.log(Level.INFO,"Entering deleteDsa ");
   	RestTemplate template = new RestTemplate();
	Map<String, String> parts = new HashMap<String, String>();

	parts.put("dsaid", dsaId);

    //SSLCertificateValidation.disable();
	ResponseEntity<String> response = template.getForEntity(url+"/deletedsa/{dsaid}", String.class,parts);
	ResultObject  resultObj = new ResultObject();

	resultObj.setStatus(false);
	resultObj.setMessage("Error");

    if (response.getStatusCode() == HttpStatus.OK)
    {

    	String jsonInString  = response.getBody();
    	try {
			 resultObj = mapper.readValue(jsonInString, ResultObject.class);
		} catch (IOException e) {
			  logger.log(Level.SEVERE,"Error deleteDsa "+e.getMessage());
			  resultObj.setStatus(false);
	    	  resultObj.setMessage("Mapping result json to object error: : " +e.getMessage());
			  e.printStackTrace();
		}

	}
    else
	{
    	  logger.log(Level.SEVERE,"Error deleteDsa ");
    	  resultObj.setStatus(false);
    	  resultObj.setMessage("Service call return statud code: " +response.getStatusCode().toString());
	}

    return resultObj;
    }

	@Override
	public synchronized ResultObject copyDsaTemplate(String dsaId, String userId) {

	   	logger.log(Level.INFO,"Entering copyDsaTemplate ");
	   	RestTemplate template = new RestTemplate();
		Map<String, String> parts = new HashMap<String, String>();

		parts.put("dsatempid", dsaId);
		parts.put("userid", userId);

	    //SSLCertificateValidation.disable();
		ResponseEntity<String> response = template.getForEntity(url+"/copydsatemp/{dsatempid}/user/{userid}", String.class,parts);
		ResultObject  resultObj = new ResultObject();

		resultObj.setStatus(false);
		resultObj.setMessage("Error");

	    if (response.getStatusCode() == HttpStatus.OK)
	    {

	    	String jsonInString  = response.getBody();
	    	try {
				 resultObj = mapper.readValue(jsonInString, ResultObject.class);
			} catch (IOException e) {
				  logger.log(Level.SEVERE,"Error copyDsaTemplate "+e.getMessage());
				  resultObj.setStatus(false);
		    	  resultObj.setMessage("Mapping result json to object error: : " +e.getMessage());
				  e.printStackTrace();
			}

		}
	    else
		{
	    	  logger.log(Level.SEVERE,"Error copyDsaTemplate ");
	    	  resultObj.setStatus(false);
	    	  resultObj.setMessage("Service call return statud code: " +response.getStatusCode().toString());
		}

	    return resultObj;

	}

	@Override
	public synchronized ResultObject updateDsaStatusFile(String dsaId, String dsaStatus) {
	   	 logger.log(Level.INFO,"Entering acceptDsa ");
	    	RestTemplate template = new RestTemplate();
	    	MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

	    	parts.add("dsaid", dsaId);
			parts.add("status", dsaStatus);

	    //SSLCertificateValidation.disable();
	 	ResponseEntity<String> response = template.postForEntity(url+"/updatestatusfile",parts, String.class,parts);
	 	ResultObject  resultObj = new ResultObject();

	 	resultObj.setStatus(false);
	 	resultObj.setMessage("Error");

	     if (response.getStatusCode() == HttpStatus.OK)
	     {

	     	String jsonInString  = response.getBody();
	     	try {
	 			 resultObj = mapper.readValue(jsonInString, ResultObject.class);
	 		} catch (IOException e) {
	 			  logger.log(Level.SEVERE,"Error acceptDsa "+e.getMessage());
	 			  resultObj.setStatus(false);
	 	    	  resultObj.setMessage("Mapping result json to object error: : " +e.getMessage());
	 			  e.printStackTrace();
	 		}

	 	}
	     else
	 	{
	     	  logger.log(Level.SEVERE,"Error acceptDsa ");
	     	  resultObj.setStatus(false);
	     	  resultObj.setMessage("Service call return statud code: " +response.getStatusCode().toString());
	 	}

	     return resultObj;
	     }

	@Override
	public synchronized ResultObject updateDsaStatus(String dsaId, String dsaStatus) {
	   	 logger.log(Level.INFO,"Entering acceptDsa ");
	    	RestTemplate template = new RestTemplate();
	    	MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

	    	parts.add("dsaid", dsaId);
			parts.add("status", dsaStatus);

	    //SSLCertificateValidation.disable();
	 	ResponseEntity<String> response = template.postForEntity(url+"/updatestatus",parts, String.class,parts);
	 	ResultObject  resultObj = new ResultObject();

	 	resultObj.setStatus(false);
	 	resultObj.setMessage("Error");

	     if (response.getStatusCode() == HttpStatus.OK)
	     {

	     	String jsonInString  = response.getBody();
	     	try {
	 			 resultObj = mapper.readValue(jsonInString, ResultObject.class);
	 		} catch (IOException e) {
	 			  logger.log(Level.SEVERE,"Error acceptDsa "+e.getMessage());
	 			  resultObj.setStatus(false);
	 	    	  resultObj.setMessage("Mapping result json to object error: : " +e.getMessage());
	 			  e.printStackTrace();
	 		}

	 	}
	     else
	 	{
	     	  logger.log(Level.SEVERE,"Error acceptDsa ");
	     	  resultObj.setStatus(false);
	     	  resultObj.setMessage("Service call return statud code: " +response.getStatusCode().toString());
	 	}

	     return resultObj;
	     }

	@Override
	public synchronized ResultObject mapDsa(String dsaId) {

	 	 logger.log(Level.INFO,"Entering mapDsa ");
	    	RestTemplate template = new RestTemplate();
	 	Map<String, String> parts = new HashMap<String, String>();

	 	parts.put("dsaid", dsaId);

	    //SSLCertificateValidation.disable();
	 	ResponseEntity<String> response = template.getForEntity(url+"/mapdsa/{dsaid}", String.class,parts);
	 	ResultObject  resultObj = new ResultObject();

	 	resultObj.setStatus(false);
	 	resultObj.setMessage("Error");

	     if (response.getStatusCode() == HttpStatus.OK)
	     {

	     	String jsonInString  = response.getBody();
	     	try {
	 			 resultObj = mapper.readValue(jsonInString, ResultObject.class);
	 		} catch (IOException e) {
	 			  logger.log(Level.SEVERE,"Error mapDsa "+e.getMessage());
	 			  resultObj.setStatus(false);
	 	    	  resultObj.setMessage("Mapping result json to object error: : " +e.getMessage());
	 			  e.printStackTrace();
	 		}

	 	}
	     else
	 	{
	     	  logger.log(Level.SEVERE,"Error mapDsa ");
	     	  resultObj.setStatus(false);
	     	  resultObj.setMessage("mapDsa Service call return statud code: " +response.getStatusCode().toString());
	 	}

	     return resultObj;
		/************************/

	     }

	@Override
	public synchronized ResultObject analyseDsa(String dsaId) {
	   	 logger.log(Level.INFO,"Entering analyseDsa ");
	    	RestTemplate template = new RestTemplate();
	 	Map<String, String> parts = new HashMap<String, String>();

	 	parts.put("dsaid", dsaId);

	    //SSLCertificateValidation.disable();
	 	ResponseEntity<String> response = template.getForEntity(url+"/analyse/{dsaid}", String.class,parts);
	 	ResultObject  resultObj = new ResultObject();

	 	resultObj.setStatus(false);
	 	resultObj.setMessage("Error");

	     if (response.getStatusCode() == HttpStatus.OK)
	     {

	     	String jsonInString  = response.getBody();
	     	try {
	 			 resultObj = mapper.readValue(jsonInString, ResultObject.class);
	 		} catch (IOException e) {
	 			  logger.log(Level.SEVERE,"Error analyseDsa "+e.getMessage());
	 			  resultObj.setStatus(false);
	 	    	  resultObj.setMessage("Mapping result json to object error: : " +e.getMessage());
	 			  e.printStackTrace();
	 		}

	 	}
	     else
	 	{
	     	  logger.log(Level.SEVERE,"Error analyseDsa ");
	     	  resultObj.setStatus(false);
	     	  resultObj.setMessage("Service call return statud code: " +response.getStatusCode().toString());
	 	}

	     return resultObj;
	     }

	@Override
	public synchronized ResultObject getDsaConflicts(String dsaId) {

	   	logger.log(Level.INFO,"Entering getDsaConflicts ");
	   	RestTemplate template = new RestTemplate();
		Map<String, String> parts = new HashMap<String, String>();

		parts.put("dsaid", dsaId);

	    //SSLCertificateValidation.disable();

		ResponseEntity<String> response = template.getForEntity(url+"/getconflict/{dsaid}", String.class,parts);
		ResultObject  resultObj = new ResultObject();

		resultObj.setStatus(false);
		resultObj.setMessage("Error");

	    if (response.getStatusCode() == HttpStatus.OK)
	    {

	    	String jsonInString  = response.getBody();
	    	try {
				 resultObj = mapper.readValue(jsonInString, ResultObject.class);
			} catch (IOException e) {
				  logger.log(Level.SEVERE,"Error getDsaConflicts "+e.getMessage());
				  resultObj.setStatus(false);
		    	  resultObj.setMessage("Mapping result json to object error: : " +e.getMessage());
				  e.printStackTrace();
			}

		}
	    else
		{
	    	  logger.log(Level.SEVERE,"Error getDsaConflicts ");
	    	  resultObj.setStatus(false);
	    	  resultObj.setMessage("Service call return statud code: " +response.getStatusCode().toString());
		}

	    return resultObj;
	    }

	@Override
	public ResultObject revokeDsa(String dsaId) {


	   	logger.log(Level.INFO,"Entering revokeDsa ");
	    RestTemplate template = new RestTemplate();
	 	Map<String, String> parts = new HashMap<String, String>();

	 	parts.put("dsaid", dsaId);

	    //SSLCertificateValidation.disable();
	 	ResponseEntity<String> response = template.getForEntity(url+"/revoke/{dsaid}", String.class,parts);
	 	ResultObject  resultObj = new ResultObject();

	 	resultObj.setStatus(false);
	 	resultObj.setMessage("Error");

	     if (response.getStatusCode() == HttpStatus.OK)
	     {

	     	String jsonInString  = response.getBody();
	     	try {
	 			 resultObj = mapper.readValue(jsonInString, ResultObject.class);
	 		} catch (IOException e) {
	 			  logger.log(Level.SEVERE,"Error revokeDsa "+e.getMessage());
	 			  resultObj.setStatus(false);
	 	    	  resultObj.setMessage("revokeDsa Mapping result json to object error: " +e.getMessage());
	 			  e.printStackTrace();
	 		}

	 	}
	     else
	 	{
	     	  logger.log(Level.SEVERE,"Error revokeDsa ");
	     	  resultObj.setStatus(false);
	     	  resultObj.setMessage("Service call return statud code: " +response.getStatusCode().toString());
	 	}

	     return resultObj;

	}
	@Override
	public ResultObject addDsaToUser(String dsaId, String userId) {

	   	logger.log(Level.INFO,"Entering addDsaToUser ");
	    RestTemplate template = new RestTemplate();
	    MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();


		parts.add("username", userId);
		 parts.add("dsaid", dsaId);

	    //SSLCertificateValidation.disable();
	 	ResponseEntity<String> response = template.postForEntity(url+"/setuserdsa",parts, String.class,parts);
	 	ResultObject  resultObj = new ResultObject();

	 	resultObj.setStatus(false);
	 	resultObj.setMessage("Error");

	     if (response.getStatusCode() == HttpStatus.OK)
	     {

	     	String jsonInString  = response.getBody();
	     	try {
	 			 resultObj = mapper.readValue(jsonInString, ResultObject.class);
	 		} catch (IOException e) {
	 			  logger.log(Level.SEVERE,"Error addDsaToUser "+e.getMessage());
	 			  resultObj.setStatus(false);
	 	    	  resultObj.setMessage("addDsaToUser error: : " +e.getMessage());
	 			  e.printStackTrace();
	 		}

	 	}
	     else
	 	{
	     	  logger.log(Level.SEVERE,"Error addDsaToUser ");
	     	  resultObj.setStatus(false);
	     	  resultObj.setMessage("Service call return status code: " +response.getStatusCode().toString());
	 	}

	     return resultObj;
	     }
    @Override
    public ResultObject getShowRaw(String dsaId) {
        logger.log(Level.INFO,"Entering getShowRaw ");
        RestTemplate template = new RestTemplate();
        Map<String, String> parts = new HashMap<String, String>();

        parts.put("dsaid", dsaId);

        //SSLCertificateValidation.disable();
        ResponseEntity<String> response = template.getForEntity(url+"/getDSA/{dsaid}", String.class,parts);
        ResultObject  resultObj = new ResultObject();

        resultObj.setStatus(false);
        resultObj.setMessage("Error");

        if (response.getStatusCode() == HttpStatus.OK)
        {
            String dsaRaw = new String(response.getBody());
            resultObj.setMessage(dsaRaw);
            //logger.log(Level.INFO,"DSA xml="+dsaRaw);
        }
        else
        {
              logger.log(Level.SEVERE,"Error getShowRaw ");
              resultObj.setStatus(false);
              resultObj.setMessage("Service call return status code: " +response.getStatusCode().toString());
        }

        return resultObj;
    }


}
