package com.atos.dsalm.objects;
import java.io.Serializable;

public class UserDetails implements Serializable {
	
	  private String userName;
	  private String userRole;
	  private String userStatus;
	  private String userId;
	  
	  public UserDetails() {
	  }

	  public UserDetails(String userName, String userRole, String userStatus,String userId) {
		  
	    this.setUserName(userName);
	    this.setUserRole(userRole);
	    this.setUserStatus(userStatus);
	    this.setUserId(userId);
	  }

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userRole
	 */
	public String getUserRole() {
		return userRole;
	}

	/**
	 * @param userRole the userRole to set
	 */
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	/**
	 * @return the userStatus
	 */
	public String getUserStatus() {
		return userStatus;
	}

	/**
	 * @param userStatus the userStatus to set
	 */
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}






}
