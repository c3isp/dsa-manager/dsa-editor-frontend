package com.atos.dsalm;

import java.util.HashMap;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Configurator {
    
    @Value("${dsalm.dsaapi.url}")
    private String dsaApiUrl;
    
    @Value("${dsalm.db.url}")
    private String dbUrl;
    
    @Value("${dsalm.db.username}")
    private String dbUsername;
    
    @Value("${dsalm.db.password}")
    private String dbPassword;
    
    @Value("${dsalm.dsaat.url}")
    private String dsaAtUrl;
    
    @Value("${dsalm.dsaeditorfe.url}")
    private String dsaEditorFeUrl;
    
    private final static Logger LOGGER = LoggerFactory.getLogger(Configurator.class);

    private final HashMap<String, String> properties;
    
    public Configurator() {
        properties = new HashMap<>();
    }
    
    @PostConstruct
    public void init() {
        addProperty("dsalm.dsaapi.url", dsaApiUrl);
        addProperty("dsalm.db.url", dbUrl);
        addProperty("dsalm.db.username", dbUsername);
        addProperty("dsalm.db.password", dbPassword);
        addProperty("dsalm.dsaat.url", dsaAtUrl);
        addProperty("dsalm.dsaeditorfe.url", dsaEditorFeUrl);
    }
    
    public String getProperty(String name) {
        return properties.getOrDefault(name, null);
    }

    private void addProperty(String name, String value) {
        properties.put(name, value);
        if (Pattern.matches(".*password.*", name)) {
            value = "********";
        }
        LOGGER.info(String.format("Set cfg property '%s' = '%s'", name, value));
    }

}
