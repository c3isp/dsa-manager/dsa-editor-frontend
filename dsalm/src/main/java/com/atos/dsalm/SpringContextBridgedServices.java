package com.atos.dsalm;

/*
 * This interface represents a list of Spring Beans (services) which need to be referenced from a non Spring class.
 */
public interface SpringContextBridgedServices {

    Configurator getConfigurator();

}
