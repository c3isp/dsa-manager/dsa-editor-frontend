package com.atos.dsalm.authentication;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.atos.dsalm.Configurator;
import com.atos.dsalm.SpringContextBridge;
import com.atos.dsalm.objects.UserDetails;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.annotation.VaadinSessionScope;

/**
 * Default mock implementation of {@link AccessControl}. This implementation
 * accepts any string as a password, and considers the user "admin" as the only
 * administrator.
 */

public class BasicAccessControl implements AccessControl {

	Logger logger = Logger.getLogger("BasicAccessControl.class");

	//String dbURL = "jdbc:mysql://localhost:3306/dsarepo"; //TODO: move to config file
	//String username = "dsarepouser"; //TODO: move to config file
	//String password = "MyPWDusr18!!"; //TODO: move to config file

    // get configurator for properties
    private Configurator cfg = SpringContextBridge.services().getConfigurator();
    String dbURL    = cfg.getProperty("dsalm.db.url");
    String username = cfg.getProperty("dsalm.db.username");
    String password = cfg.getProperty("dsalm.db.password");
    
	@Override
	public boolean signIn(String username, String password) {
		Boolean returnStatus = false;
		if (username == null || username.isEmpty())
		{
			returnStatus = false;
		}
		else if(password == null || password.isEmpty())
		{
			returnStatus = false;
		}
		else
		{
			UserDetails myUser = loginUser(username,password);
			if(myUser != null && myUser.getUserStatus().trim().equals("ok"))
			{
				CurrentUser.set(myUser);
				returnStatus = true;
			}
			else
			{
				returnStatus = false;
			}
		}



		return returnStatus;
	}

	@Override
	public boolean isUserSignedIn() {
		if (CurrentUser.get() != null)
			return true;
		else
			return false;
	}

	@Override
	public boolean isUserInRole(String role) {
		if ("admin".equals(role)) {
			// Only the "admin" user is in the "admin" role
			// return getPrincipalName().equals("admin");
		}

		// All users are in all non-admin roles
		return true;
	}

	@Override
	public UserDetails getPrincipalName() {
		return CurrentUser.get();
	}

	private UserDetails loginUser(String userName, String userPassword) {
		UserDetails user = new UserDetails();
		String myUserID = null;
		String myUserRole = null;
		Connection conn=null;
		logger.log(Level.INFO,"***********Entering loginUser Operations with ID: "+userName+"***********");

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");

			conn = DriverManager.getConnection(dbURL, username, password);

			String sql = "SELECT userId, userRole FROM dsarepo.user WHERE userName = ? AND userPass = ?";

			PreparedStatement statement = conn.prepareStatement(sql);
			statement.setString(1, userName.trim());
			statement.setString(2, userPassword.trim());

			ResultSet result = statement.executeQuery();

			while (result.next()){
				myUserID = result.getString(1).trim();
				myUserRole = result.getString(2).trim();
			}

			if(myUserID!=null && myUserRole!=null)
			{

				user = new UserDetails(userName,myUserRole,"ok",myUserID);
			}
			else
			{
				user = new UserDetails("no such a user","no such a role","fail","no such ID");
			}



		} catch (Exception e) {
			user = new UserDetails("no such a user",e.getMessage(),"fail","no such ID");
			logger.log(Level.SEVERE, "Getting user from DB failed" + e.getMessage());
		} finally {
			if(conn != null) {
				try{conn.close();} catch(Exception e) {}
			}
		}

		return user;
	}

}
