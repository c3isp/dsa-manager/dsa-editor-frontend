package com.atos.dsalm.authentication;

import com.atos.dsalm.objects.UserDetails;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.annotation.VaadinSessionScope;

/**
 * Simple interface for authentication and authorization checks.
 */

public interface AccessControl {

    public boolean signIn(String username, String password);

    public boolean isUserSignedIn();

    public boolean isUserInRole(String role);

    public UserDetails getPrincipalName();
}
